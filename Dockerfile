FROM alpine AS common-stage
RUN echo 'Common stage!' > /test

FROM alpine AS target-1
COPY --from=common-stage /test /test
RUN echo 'Target 1!'

FROM alpine AS target-2
COPY --from=common-stage /test /test
RUN echo 'Target 2!'
